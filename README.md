Ansible Archlinux Nginx
=======================

A role to install nginx and set basic  config
With Php Fpm

Dependencies
------------

[Requirements](molecule/default/requirements.yml)


Variables
---------

[Role Variables](defaults/main.yml)


Example Playbook
----------------

[Test Playbook](molecule/default/converge.yml)


License
-------

[License](LICENSE)


Todo
----
- Be Kind
- Add Ssl Management
